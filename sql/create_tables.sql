-- Spring Session
CREATE TABLE spring_session (
	primary_id CHAR(36) NOT NULL,
	session_id CHAR(36) NOT NULL,
	creation_time BIGINT NOT NULL,
	last_access_time BIGINT NOT NULL,
	max_inactive_interval INT NOT NULL,
	expiry_time BIGINT NOT NULL,
	principal_name VARCHAR(100),
	CONSTRAINT spring_session_pk PRIMARY KEY (primary_id)
);

CREATE UNIQUE INDEX spring_session_ix1 ON spring_session (session_id);
CREATE INDEX spring_session_ix2 ON spring_session (expiry_time);
CREATE INDEX spring_session_ix3 ON spring_session (principal_name);

CREATE TABLE spring_session_attributes (
	session_primary_id CHAR(36) NOT NULL,
	attribute_name VARCHAR(200) NOT NULL,
	attribute_bytes BYTEA NOT NULL,
	CONSTRAINT spring_session_attributes_pk PRIMARY KEY (session_primary_id, attribute_name),
	CONSTRAINT spring_session_attributes_fk FOREIGN KEY (session_primary_id) REFERENCES spring_session(primary_id) ON DELETE CASCADE
);

-- アーティスト
CREATE TABLE public.artist
(
    id BIGINT NOT NULL,
    name character varying(100) NOT NULL,
    home_page_url character varying(500) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT artist_uk1 UNIQUE (name)

)
WITH (
    OIDS = FALSE
);
CREATE SEQUENCE public.artist_seq;

-- 会場
CREATE TABLE public.venue
(
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    address character varying(500) NOT NULL,
    home_page_url character varying(500) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT venue_uk1 UNIQUE (name)

)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.venue_seq;

-- ライブ
CREATE TABLE public.live
(
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    venue_id bigint,
    home_page_url character varying(500) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT live_uk1 UNIQUE (name)

)
WITH (
    OIDS = FALSE
);
CREATE SEQUENCE public.live_seq;

-- 公演
CREATE TABLE public.performance
(
    id bigint NOT NULL,
    live_id bigint NOT NULL,
    date date NOT NULL,
    open_time time(0) without time zone NOT NULL,
    start_time time(0) without time zone NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT performance_uk1 UNIQUE (live_id, date, open_time)

)
WITH (
    OIDS = FALSE
);
CREATE SEQUENCE public.performance_seq;




CREATE TABLE public.users
(
    id bigint NOT NULL,
    login_id character varying(20) NOT NULL,
    password character varying(60) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT users_uk1 UNIQUE (login_id)

)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.test_table
(
    id bigint NOT NULL,
    code character varying(5) NOT NULL,
    name character varying(20) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT test_table_uk1 UNIQUE (code)

)
WITH (
    OIDS = FALSE
);