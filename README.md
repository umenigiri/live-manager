# live-manager

## TODO
- ライブの登録
    - まずは基本情報（名前、会場）の登録
    - 公演を登録していく
- 権限管理

## DONE
* パスパラメータの値はテンプレートからも参照できそう
* MutableArtist いらなさそう
* Domain オブジェクトのフィールド名から冗長性を除去
* 会場の更新
* メッセージを一箇所で管理して文言の統一をしたい
* メニュー
* 会場の登録
* DB の更新/検索ができるようになる
* Thymeleaf で画面が表示できるようになる
* 認証ができるようになる
