package lm.domain.live.modify;

import lm.domain.DomainMessage;
import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;
import org.eclipse.collections.impl.factory.Lists;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class PerformancesTest {

    @Test
    void 同じ日付_開演時間の公演を追加すると例外がスローされる() {
        Performances performances = new Performances(Lists.immutable.of());
        performances.add(performance("2019-01-01", "12:00", "13:00"));
        
        assertThatThrownBy(() -> performances.add(performance("2019-01-01", "12:30", "13:00")))
                .isInstanceOf(DomainException.class)
                .hasMessage(DomainMessage.PERFORMANCE_KEY_IS_DUPLICATED.build());
    }

    @Test
    void 同じIDの公演の情報を書き換えられる() {
        // setup
        Performances performances = new Performances(Lists.immutable.of());
        performances.add(performance(1, "2019-01-01", "12:00", "13:00"));
        performances.add(performance(2, "2019-01-02", "13:00", "14:00"));
        
        // exercise
        Performance modified = performance(2, "2019-02-01", "01:00", "02:00");
        performances.modify(modified);
        
        // verify
        Performance actual = performances.get(new Id<>(2));
        assertThat(actual.getDate()).isEqualTo(modified.getDate());
        assertThat(actual.getOpenTime()).isEqualTo(modified.getOpenTime());
        assertThat(actual.getStartTime()).isEqualTo(modified.getStartTime());
    }

    @Test
    void 同じIDの公演が存在しない場合はエラー() {
        // setup
        Performances performances = new Performances(Lists.immutable.of());
        performances.add(performance(1, "2019-01-01", "12:00", "13:00"));
        performances.add(performance(2, "2019-01-02", "13:00", "14:00"));

        // exercise
        Performance modified = performance(3, "2019-02-01", "01:00", "02:00");
        assertThatThrownBy(() -> performances.modify(modified))
                .isInstanceOf(DomainException.class)
                .hasMessage(DomainMessage.PERFORMANCE_WAS_REMOVED.build());
    }

    @Test
    void 更新した結果_同じキーとなる公演が存在した場合はエラーになり_元の公演一覧の状態は維持される() {
        // setup
        Performances performances = new Performances(Lists.immutable.of());
        performances.add(performance(1, "2019-01-01", "12:00", "13:00"));
        performances.add(performance(2, "2019-01-02", "13:00", "14:00"));

        // exercise
        Performance modified = performance(2, "2019-01-01", "12:30", "13:00");
        assertThatThrownBy(() -> performances.modify(modified))
                .isInstanceOf(DomainException.class)
                .hasMessage(DomainMessage.PERFORMANCE_KEY_IS_DUPLICATED.build());

        Performance target = performances.get(new Id<>(2));
        assertThat(target).isNotNull();
    }

    private static Performance performance(String date, String openTime, String startTime) {
        return Performance.newPerformance(PerformanceDate.parse(date), OpenTime.parse(openTime), StartTime.parse(startTime));
    }

    private static Performance performance(int id, String date, String openTime, String startTime) {
        return Performance.rebuild(new Id<>(id), PerformanceDate.parse(date), OpenTime.parse(openTime), StartTime.parse(startTime));
    }
}