package lm.domain.primitive;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class HourMinuteValueTest {

    @Test
    public void 文字列フォーマットでゼロ埋めがされることの確認() {
        HourMinuteValue value = new HourMinuteValue(1, 2);
        assertThat(value.format()).isEqualTo("01:02");
    }
}