package lm.infrastructure.database.artist;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Script;
import org.seasar.doma.Select;
import org.seasar.doma.Update;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;
import java.util.Optional;

@Dao
@ConfigAutowireable
public interface ArtistTableDao {
    @Script
    void lockTable();
    
    @Select
    List<ArtistTable> findAll();
    
    @Select
    ArtistTable find(long id);
    
    @Select
    Optional<ArtistTable> findByName(String name);

    @Select
    Optional<ArtistTable> findByNameWithoutId(long ignoreId, String name);
    
    @Insert
    int insert(ArtistTable artistTable);
    
    @Update
    int update(ArtistTable artistTable);
}
