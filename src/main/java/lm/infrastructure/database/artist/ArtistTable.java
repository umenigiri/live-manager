package lm.infrastructure.database.artist;

import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.SequenceGenerator;
import org.seasar.doma.Table;

@Entity
@Table(name = "artist")
public class ArtistTable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequence = "artist_seq")
    Long id;
    String name;
    String homePageUrl;

    @Override
    public String toString() {
        return "ArtistTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }
}
