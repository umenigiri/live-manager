package lm.infrastructure.database.artist;

import lm.domain.Id;
import lm.domain.master.artist.Artist;
import lm.domain.master.artist.ArtistHomePageUrl;
import lm.domain.master.artist.ArtistName;
import lm.domain.master.artist.ArtistRepository;
import lm.domain.master.artist.Artists;
import lm.domain.master.artist.NewArtist;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.stereotype.Repository;

@Repository
public class RdbArtistRepository implements ArtistRepository {
    private final ArtistTableDao artistTableDao;

    public RdbArtistRepository(ArtistTableDao artistTableDao) {
        this.artistTableDao = artistTableDao;
    }

    @Override
    public boolean existsSameNameArtist(ArtistName artistName) {
        this.artistTableDao.lockTable();
        return this.artistTableDao.findByName(artistName.getValue()).isPresent();
    }

    @Override
    public boolean existsSameNameArtist(Id<Artist> id, ArtistName artistName) {
        this.artistTableDao.lockTable();
        return this.artistTableDao.findByNameWithoutId(id.getValue(), artistName.getValue()).isPresent();
    }

    @Override
    public Artists findAll() {
        return new Artists(
            Lists.immutable
                .ofAll(this.artistTableDao.findAll())
                .collect(this::toArtist)
        );
    }

    @Override
    public Artist findById(Id<Artist> id) {
        ArtistTable table = this.artistTableDao.find(id.getValue());
        return this.toArtist(table);
    }

    private Artist toArtist(ArtistTable table) {
        Id<Artist> id = new Id<>(table.id);
        ArtistName artistName = new ArtistName(table.name);
        ArtistHomePageUrl artistHomePageUrl = new ArtistHomePageUrl(table.homePageUrl);
        return new Artist(id, artistName, artistHomePageUrl);
    }

    @Override
    public void register(NewArtist newArtist) {
        ArtistTable artistTable = new ArtistTable();
        
        artistTable.name = newArtist.getName().getValue();
        artistTable.homePageUrl = newArtist.getHomePageUrl().getValue();
        
        this.artistTableDao.insert(artistTable);
    }

    @Override
    public void modify(Artist artist) {
        ArtistTable artistTable = this.artistTableDao.find(artist.getId().getValue());
        
        artistTable.name = artist.getName().getValue();
        artistTable.homePageUrl = artist.getHomePageUrl().getValue();
        
        this.artistTableDao.update(artistTable);
    }
}
