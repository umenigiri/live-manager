package lm.infrastructure.database;

import org.seasar.doma.boot.autoconfigure.DomaConfigBuilder;
import org.seasar.doma.jdbc.JdbcLogger;
import org.seasar.doma.jdbc.UtilLoggingJdbcLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Level;

@Configuration
public class DomaConfig {
    
    @Bean
    public JdbcLogger jdbcLogger() {
        return new UtilLoggingJdbcLogger(Level.FINE);
    }
    
    @Bean
    public DomaConfigBuilder domaConfigBuilder() {
        DomaConfigBuilder builder = new DomaConfigBuilder();
        builder.jdbcLogger(this.jdbcLogger());
        return builder;
    }
}
