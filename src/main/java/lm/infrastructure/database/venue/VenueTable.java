package lm.infrastructure.database.venue;

import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.SequenceGenerator;
import org.seasar.doma.Table;

@Entity
@Table(name = "venue")
public class VenueTable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequence = "venue_seq")
    Long id;
    String name;
    String address;
    String homePageUrl;

    @Override
    public String toString() {
        return "VenueTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }
}
