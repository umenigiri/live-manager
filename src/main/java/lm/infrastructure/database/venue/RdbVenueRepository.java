package lm.infrastructure.database.venue;

import lm.domain.Id;
import lm.domain.master.venue.AllVenues;
import lm.domain.master.venue.NewVenue;
import lm.domain.master.venue.Venue;
import lm.domain.master.venue.VenueAddress;
import lm.domain.master.venue.VenueHomePageUrl;
import lm.domain.master.venue.VenueName;
import lm.domain.master.venue.VenueRepository;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.stereotype.Repository;

@Repository
public class RdbVenueRepository implements VenueRepository {
    
    private final VenueTableDao venueTableDao;

    public RdbVenueRepository(VenueTableDao venueTableDao) {
        this.venueTableDao = venueTableDao;
    }

    @Override
    public boolean existsSameNameVenue(VenueName venueName) {
        this.venueTableDao.lockTable();
        return this.venueTableDao.findByName(venueName.getValue()) != null;
    }

    @Override
    public boolean existsSameNameVenue(VenueName venueName, Id<Venue> excludeId) {
        this.venueTableDao.lockTable();
        return this.venueTableDao.findByNameWithoutId(venueName.getValue(), excludeId.getValue()) != null;
    }

    @Override
    public Venue findById(Id<Venue> id) {
        return this.toVenue(this.venueTableDao.findById(id.getValue()));
    }

    @Override
    public AllVenues findAll() {
        return new AllVenues(Lists.immutable.ofAll(this.venueTableDao.findAll()).collect(this::toVenue));
    }

    @Override
    public void register(NewVenue newVenue) {
        VenueTable table = new VenueTable();
        
        table.name = newVenue.getName().getValue();
        table.address = newVenue.getAddress().getValue();
        table.homePageUrl = newVenue.getHomePageUrl().getValue();
        
        this.venueTableDao.insert(table);
    }

    @Override
    public void modify(Venue venue) {
        VenueTable table = this.venueTableDao.findById(venue.getId().getValue());
        
        table.name = venue.getName().getValue();
        table.address = venue.getAddress().getValue();
        table.homePageUrl = venue.getHomePageUrl().getValue();
        
        this.venueTableDao.update(table);
    }

    private Venue toVenue(VenueTable table) {
        Id<Venue> id = new Id<>(table.id);
        VenueName venueName = new VenueName(table.name);
        VenueAddress venueAddress = new VenueAddress(table.address);
        VenueHomePageUrl venueHomePageUrl = new VenueHomePageUrl(table.homePageUrl);
        return new Venue(id, venueName, venueAddress, venueHomePageUrl);
    }
}
