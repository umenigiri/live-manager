package lm.infrastructure.database.venue;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Script;
import org.seasar.doma.Select;
import org.seasar.doma.Update;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;

@Dao
@ConfigAutowireable
public interface VenueTableDao {
    @Script
    void lockTable();
    
    @Select
    VenueTable findById(long id);
    
    @Select
    VenueTable findByName(String name);

    @Select
    VenueTable findByNameWithoutId(String name, long excludeId);
    
    @Select
    List<VenueTable> findAll();
    
    @Insert
    int insert(VenueTable venueTable);
    
    @Update
    int update(VenueTable venueTable);
}
