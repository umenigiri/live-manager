package lm.infrastructure.database;

import org.seasar.doma.jdbc.Sql;
import org.seasar.doma.jdbc.UtilLoggingJdbcLogger;

import java.util.function.Supplier;
import java.util.logging.Level;

public class CustomDomaLogger extends UtilLoggingJdbcLogger {
    
    public CustomDomaLogger() {
        super(Level.FINEST);
    }

    @Override
    protected void logSql(String callerClassName, String callerMethodName, Sql<?> sql, Level level, Supplier<String> messageSupplier) {
        super.logSql(callerClassName, callerMethodName, sql, Level.FINE, messageSupplier);
    }
}
