package lm.infrastructure.database;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.Optional;

@Dao
@ConfigAutowireable
public interface UsersDao {
    @Select
    Optional<Users> findByLoginId(String loginId);
}
