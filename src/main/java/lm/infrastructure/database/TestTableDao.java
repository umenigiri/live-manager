package lm.infrastructure.database;

import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;

@Dao
@ConfigAutowireable
public interface TestTableDao {
    @Select
    List<TestTable> findAll();
    @Select
    TestTable find(long id);
    @Insert
    int insert(TestTable testTable);
    @Update
    int update(TestTable testTable);
    @Delete
    int delete(TestTable testTable);
}
