package lm.infrastructure.database.live;

import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.SequenceGenerator;
import org.seasar.doma.Table;

import java.util.Optional;

@Entity
@Table(name = "live")
public class LiveTable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequence = "live_seq")
    Long id;
    String name;
    Long venueId;
    String homePageUrl;
    
    public Optional<Long> getVenueId() {
        return Optional.ofNullable(this.venueId);
    }

    @Override
    public String toString() {
        return "LiveTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", venueId=" + venueId +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }
}
