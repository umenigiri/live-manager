package lm.infrastructure.database.live;

import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;

@Dao
@ConfigAutowireable
public interface PerformanceTableDao {
    
    @Insert
    int insert(PerformanceTable performanceTable);
    
    @Select
    List<PerformanceTable> findByLiveId(long liveId);

    @Select
    PerformanceTable findById(long id);
    
    @Delete
    int delete(PerformanceTable performanceTable);

    @Update
    int update(PerformanceTable table);
}
