package lm.infrastructure.database.live;

import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.SequenceGenerator;
import org.seasar.doma.Table;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "performance")
public class PerformanceTable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequence = "performance_seq")
    Long id;
    Long liveId;
    LocalDate date;
    LocalTime openTime;
    LocalTime startTime;

    @Override
    public String toString() {
        return "PerformanceTable{" +
                "id=" + id +
                ", liveId=" + liveId +
                ", date=" + date +
                ", openTime=" + openTime +
                ", startTime=" + startTime +
                '}';
    }
}
