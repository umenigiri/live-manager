package lm.infrastructure.database.live;

import lm.domain.Id;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;
import lm.domain.live.LiveRepository;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;
import lm.domain.live.modify.AllLives;
import lm.domain.live.modify.Live;
import lm.domain.live.modify.LiveAttributes;
import lm.domain.live.modify.Performance;
import lm.domain.live.modify.Performances;
import lm.domain.live.register.NewLive;
import lm.domain.master.venue.Venue;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.seasar.doma.jdbc.SelectOptions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RdbLiveRepository implements LiveRepository {
    
    private final LiveTableDao liveTableDao;
    private final PerformanceTableDao performanceTableDao;

    public RdbLiveRepository(LiveTableDao liveTableDao, PerformanceTableDao performanceTableDao) {
        this.liveTableDao = liveTableDao;
        this.performanceTableDao = performanceTableDao;
    }

    @Override
    public boolean existsSameNameLive(LiveName name) {
        this.liveTableDao.lockTable();
        return this.liveTableDao.findByName(name.getValue()) != null;
    }

    @Override
    public boolean existsSameNameLive(LiveName name, Id<Live> excludeId) {
        this.liveTableDao.lockTable();
        return this.liveTableDao.findByNameWithoutId(name.getValue(), excludeId.getValue()) != null;
    }

    @Override
    public Id<Live> register(NewLive newLive) {
        LiveTable table = new LiveTable();
        table.name = newLive.getName().getValue();
        table.homePageUrl = newLive.getHomePageUrl().getValue();
        table.venueId = newLive.getVenueId().map(Id::getValue).orElse(null);
        
        this.liveTableDao.insert(table);
        
        return new Id<>(table.id);
    }

    @Override
    public Live search(Id<Live> id) {
        return toLive(this.liveTableDao.findById(id.getValue()));
    }

    @Override
    public Live searchWithLock(Id<Live> id) {
        return this.toLive(this.liveTableDao.findById(id.getValue(), SelectOptions.get().forUpdate()));
    }

    private Live toLive(LiveTable liveTable) {
        Id<Live> id = new Id<>(liveTable.id);

        LiveAttributes liveAttributes = this.toLiveAttribute(liveTable);

        List<PerformanceTable> performanceTableList = this.performanceTableDao.findByLiveId(liveTable.id);
        Performances performances = this.toPerformances(performanceTableList);

        return new Live(id, liveAttributes, performances);
    }
    
    private LiveAttributes toLiveAttribute(LiveTable liveTable) {
        LiveName liveName = new LiveName(liveTable.name);
        LiveHomePageUrl liveHomePageUrl = new LiveHomePageUrl(liveTable.homePageUrl);
        Id<Venue> venueId = liveTable.getVenueId().map(Id<Venue>::new).orElse(null);
        return new LiveAttributes(liveName, liveHomePageUrl, venueId);
    }
    
    private Performances toPerformances(List<PerformanceTable> performanceTableList) {
        return new Performances(Lists.immutable.ofAll(performanceTableList).collect(this::toPerformance));
    }
    
    private Performance toPerformance(PerformanceTable table) {
        Id<Performance> id = new Id<>(table.id);
        PerformanceDate performanceDate = new PerformanceDate(table.date);
        OpenTime openTime = new OpenTime(table.openTime);
        StartTime startTime = new StartTime(table.startTime);
        return Performance.rebuild(id, performanceDate, openTime, startTime);
    }

    @Override
    public void modify(Id<Live> liveId, LiveAttributes liveAttributes) {
        LiveTable table = this.liveTableDao.findById(liveId.getValue());
        table.name = liveAttributes.getName().getValue();
        table.homePageUrl = liveAttributes.getHomePageUrl().getValue();
        table.venueId = liveAttributes.getVenueId().map(Id::getValue).orElse(null);
        
        this.liveTableDao.update(table);
    }

    @Override
    public void addPerformance(Id<Live> id, Performance performance) {
        PerformanceTable table = new PerformanceTable();
        table.liveId = id.getValue();
        table.date = performance.getDate().getValue();
        table.openTime = performance.getOpenTime().toLocalTime();
        table.startTime = performance.getStartTime().toLocalTime();
        
        this.performanceTableDao.insert(table);
    }

    @Override
    public AllLives getAllLives() {
        ImmutableList<Live> list = Lists.immutable.ofAll(this.liveTableDao.findAll()).collect(this::toLive);
        return new AllLives(list);
    }

    @Override
    public void removePerformance(Id<Performance> performanceId) {
        PerformanceTable table = this.performanceTableDao.findById(performanceId.getValue());
        this.performanceTableDao.delete(table);
    }

    @Override
    public void modifyPerformance(Performance performance) {
        PerformanceTable table = this.performanceTableDao.findById(performance.getId().getValue());
        table.date = performance.getDate().getValue();
        table.openTime = performance.getOpenTime().toLocalTime();
        table.startTime = performance.getStartTime().toLocalTime();
        
        this.performanceTableDao.update(table);
    }
}
