package lm.infrastructure.database.live;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Script;
import org.seasar.doma.Select;
import org.seasar.doma.Update;
import org.seasar.doma.boot.ConfigAutowireable;
import org.seasar.doma.jdbc.SelectOptions;

import java.util.List;

@Dao
@ConfigAutowireable
public interface LiveTableDao {
    
    @Script
    void lockTable();
    
    @Select
    LiveTable findByName(String name);

    @Select
    LiveTable findByNameWithoutId(String name, long excludeId);
    
    @Insert
    int insert(LiveTable liveTable);
    
    @Update
    int update(LiveTable liveTable);
    
    @Select
    LiveTable findById(long id);

    @Select
    LiveTable findById(long id, SelectOptions selectOptions);
    
    @Select
    List<LiveTable> findAll();
}
