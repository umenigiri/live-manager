package lm.domain;

import java.util.Objects;

public enum DomainMessage {
    /**
     * アーティスト名がすでに使用されています。
     */
    ARTIST_NAME_IS_ALREADY_USED("アーティスト名がすでに使用されています。"),

    /**
     * 会場名がすでに使用されています。
     */
    VENUE_NAME_IS_ALREADY_USED("会場名がすでに使用されています。"),

    /**
     * 開場時間は開演時間より前にしてください
     */
    OPEN_DATE_TIME_MUST_BE_BEFORE_THAN_START_DATE_TIME("開場時間は開演時間より前にしてください。"),

    /**
     * ライブ名がすでに使用されています。
     */
    LIVE_NAME_IS_ALREADY_USED("ライブ名がすでに使用されています。"),

    /**
     * 日付・開演時間が同じ公演がすでに存在します。
     */
    PERFORMANCE_KEY_IS_DUPLICATED("日付・開演時間が同じ公演がすでに存在します。"),

    /**
     * 公演が存在しません。すでに削除されている可能性があります。
     */
    PERFORMANCE_WAS_REMOVED("公演が存在しません。すでに削除されている可能性があります。"),
    ;

    /**
     * アーティストのホームページの URL は {max} 文字以下で入力してください。
     */
    public static final String INVALID_ARTIST_HOME_PAGE_URL_LENGTH = "アーティストのホームページの URL は {max} 文字以下で入力してください。";

    /**
     * アーティスト名は {max} 文字以下で入力してください。
     */
    public static final String INVALID_ARTIST_NAME_LENGTH = "アーティスト名は {max} 文字以下で入力してください。";

    /**
     * 会場の住所は {max} 文字以下で入力してください。
     */
    public static final String INVALID_VENUE_ADDRESS_LENGTH = "会場の住所は {max} 文字以下で入力してください。";

    /**
     * 会場のホームページの URL は {max} 文字以下で入力してください。
     */
    public static final String INVALID_VENUE_HOME_PAGE_URL_LENGTH = "会場のホームページの URL は {max} 文字以下で入力してください。";

    /**
     * 会場名は {max} 文字以下で入力してください。
     */
    public static final String INVALID_VENUE_NAME_LENGTH = "会場名は {max} 文字以下で入力してください。";

    /**
     * アーティスト名は必須です。
     */
    public static final String ARTIST_NAME_IS_REQUIRED = "アーティスト名は必須です。";

    /**
     * 会場名は必須です。
     */
    public static final String VENUE_NAME_IS_REQUIRED = "会場名は必須です。";

    /**
     * ライブ名は必須です。
     */
    public static final String LIVE_NAME_IS_REQUIRED = "ライブ名は必須です。";
    
    /**
     * ライブ名は {max} 文字以下で入力してください。
     */
    public static final String INVALID_LIVE_NAME_LENGTH = "ライブ名は {max} 文字以下で入力してください。";

    /**
     * ライブのホームページの URL は {max} 文字以下で入力してください。
     */
    public static final String INVALID_LIVE_HOME_PAGE_URL_LENGTH = "ライブのホームページの URL は {max} 文字以下で入力してください。";

    /**
     * 公演日は必須です。
     */
    public static final String PERFORMANCE_DATE_IS_REQUIRED = "公演日は必須です。";
    
    /**
     * 公演日は YYYY-MM-DD の書式で入力してください。
     */
    public static final String INVALID_PERFORMANCE_DATE_PATTERN = "公演日は YYYY-MM-DD の書式で入力してください。";



    private final String template;

    DomainMessage(String template) {
        this.template = Objects.requireNonNull(template);
    }

    public String build() {
        return this.template;
    }
}
