package lm.domain.primitive;

import lm.domain.exception.DomainException;

import java.io.Serializable;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

/**
 * システムで扱う「時分」のみの値オブジェクト.
 */
public class HourMinuteValue implements Serializable {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    private final int hour;
    private final int minute;
    
    public static HourMinuteValue parse(String text) {
        try {
            LocalTime time = LocalTime.parse(text, formatter);
            return new HourMinuteValue(time.getHour(), time.getMinute());
        } catch (DateTimeParseException e) {
            throw new DomainException("時刻の書式が正しくありません。");
        }
    }

    public HourMinuteValue(int hour, int minute) {
        if (hour < 0 || 23 < hour) {
            throw new DomainException("時間は 0-23 で指定してください。");
        }
        if (minute < 0 || 59 < minute) {
            throw new DomainException("分は 0-59 で指定してください。");
        }
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public boolean isBefore(HourMinuteValue other) {
        return this.toLocalTime().isBefore(other.toLocalTime());
    }
    
    public boolean isAfter(HourMinuteValue other) {
        return this.toLocalTime().isAfter(other.toLocalTime());
    }
    
    public LocalTime toLocalTime() {
        return LocalTime.of(this.hour, this.minute);
    }
    
    public String format() {
        return String.format("%02d:%02d", this.hour, this.minute);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HourMinuteValue that = (HourMinuteValue) o;
        return hour == that.hour &&
                minute == that.minute;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hour, minute);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "hour=" + hour +
                ", minute=" + minute +
                '}';
    }
}
