package lm.domain.primitive;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * システムで扱う「日付」のみの値オブジェクト.
 */
public class DateValue implements Serializable {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd");
    private final LocalDate value;
    
    public static DateValue parse(String text) {
        return new DateValue(LocalDate.parse(text, formatter));
    }

    public DateValue(LocalDate value) {
        assertNotNull(value);
        this.value = value;
    }

    public LocalDate getValue() {
        return value;
    }
    
    public String format() {
        return this.value.format(formatter);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateValue dateValue = (DateValue) o;
        return Objects.equals(value, dateValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "value=" + value +
                '}';
    }
}
