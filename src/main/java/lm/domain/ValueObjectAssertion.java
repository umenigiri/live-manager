package lm.domain;

import java.util.Objects;

/**
 * 値オブジェクトの値検証メソッドを定義したクラス.
 */
public class ValueObjectAssertion {

    /**
     * 指定した値が {@code null} でないことを検証する.
     * @param value 検証値
     */
    public static void assertNotNull(Object value) {
        Objects.requireNonNull(value, "value must not be null.");
    }

    /**
     * 指定した値がブランクでないことを検証する.
     * @param value 検証値
     */
    public static void assertNotBlank(String value) {
        assertNotNull(value);
        
        if (value.isEmpty() || value.isBlank()) {
            throw new IllegalArgumentException("value must not be empty or blank.");
        }
    }
}
