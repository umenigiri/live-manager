package lm.domain.live;

import lm.domain.DomainMessage;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * ライブの名前.
 * <p>
 * 「アニサマ 2019」「リスアニライブ 2019」など. 
 */
public class LiveName implements Serializable {
    private final String value;

    public LiveName(String value) {
        assertNotBlank(value);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "LiveName{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LiveName liveName = (LiveName) o;
        return Objects.equals(value, liveName.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Size(max = 100, message = DomainMessage.INVALID_LIVE_NAME_LENGTH)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
