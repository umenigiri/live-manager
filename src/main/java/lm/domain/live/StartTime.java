package lm.domain.live;

import lm.domain.primitive.HourMinuteValue;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.LocalTime;

/**
 * 開演日時.
 */
public class StartTime extends HourMinuteValue implements Serializable {

    public static StartTime parse(String text) {
        HourMinuteValue value = HourMinuteValue.parse(text);
        return new StartTime(value.getHour(), value.getMinute());
    }
    
    public StartTime(int hour, int minute) {
        super(hour, minute);
    }
    
    public StartTime(LocalTime localTime) {
        this(localTime.getHour(), localTime.getMinute());
    }
    
    @Min(value = 0, message = "開演時間の時間は {value} 以上で入力してください。")
    @Max(value = 23, message = "開演時間の時間は {value} 以下で入力してください。")
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface HourValidation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @Min(value = 0, message = "開演時間の時間は {value} 以上で入力してください。")
    @Max(value = 59, message = "開演時間の時間は {value} 以下で入力してください。")
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface MinuteValidation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
