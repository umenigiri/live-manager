package lm.domain.live;

import lm.domain.Id;
import lm.domain.live.modify.AllLives;
import lm.domain.live.modify.Live;
import lm.domain.live.modify.LiveAttributes;
import lm.domain.live.modify.Performance;
import lm.domain.live.register.NewLive;

public interface LiveRepository {
    
    boolean existsSameNameLive(LiveName name);

    boolean existsSameNameLive(LiveName name, Id<Live> excludeId);
    
    Id<Live> register(NewLive newLive);
    
    Live search(Id<Live> id);
    
    Live searchWithLock(Id<Live> id);
    
    void modify(Id<Live> liveId, LiveAttributes liveAttributes);
    
    void addPerformance(Id<Live> id, Performance performance);

    AllLives getAllLives();

    void removePerformance(Id<Performance> performanceId);
    
    void modifyPerformance(Performance performance);
}
