package lm.domain.live;

import lm.domain.DomainMessage;
import lm.domain.primitive.DateValue;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.LocalDate;

/**
 * 公演日.
 */
public class PerformanceDate extends DateValue implements Serializable {

    public PerformanceDate(LocalDate value) {
        super(value);
    }
    
    public static PerformanceDate parse(String text) {
        return new PerformanceDate(DateValue.parse(text).getValue());
    }

    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = DomainMessage.INVALID_PERFORMANCE_DATE_PATTERN)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
