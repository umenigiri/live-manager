package lm.domain.live;

import lm.domain.DomainMessage;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * ライブのホームページ URL.
 */
public class LiveHomePageUrl implements Serializable {
    private final String value;

    public LiveHomePageUrl(String value) {
        assertNotNull(value);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "LiveHomePageUrl{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LiveHomePageUrl that = (LiveHomePageUrl) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Size(max = 500, message = DomainMessage.INVALID_LIVE_HOME_PAGE_URL_LENGTH)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
