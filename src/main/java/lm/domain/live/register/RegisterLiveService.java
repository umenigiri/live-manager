package lm.domain.live.register;

import lm.domain.DomainMessage;
import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.LiveRepository;
import lm.domain.live.modify.Live;
import org.springframework.stereotype.Component;

@Component
public class RegisterLiveService {
    
    private final LiveRepository liveRepository;

    public RegisterLiveService(LiveRepository liveRepository) {
        this.liveRepository = liveRepository;
    }
    
    public Id<Live> register(NewLive newLive) {
        if (this.liveRepository.existsSameNameLive(newLive.getName())) {
            throw new DomainException(DomainMessage.LIVE_NAME_IS_ALREADY_USED);
        }
        
        return this.liveRepository.register(newLive);
    }
}
