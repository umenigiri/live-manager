package lm.domain.live.register;

import lm.domain.Id;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;
import lm.domain.master.venue.Venue;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * 新しく登録されるライブ.
 */
public class NewLive implements Serializable {
    private final LiveName name;
    private final Id<Venue> venueId;
    private final LiveHomePageUrl homePageUrl;

    public NewLive(LiveName name, Id<Venue> venueId, LiveHomePageUrl homePageUrl) {
        this.name = Objects.requireNonNull(name);
        this.venueId = venueId;
        this.homePageUrl = Objects.requireNonNull(homePageUrl);
    }

    public LiveName getName() {
        return name;
    }

    public Optional<Id<Venue>> getVenueId() {
        return Optional.ofNullable(venueId);
    }

    public LiveHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }

    @Override
    public String toString() {
        return "NewLive{" +
                "name=" + name +
                ", venueId=" + venueId +
                ", homePageUrl=" + homePageUrl +
                '}';
    }
}
