package lm.domain.live.modify;

import org.eclipse.collections.api.list.ImmutableList;

import java.util.Objects;

public class AllLives {
    private final ImmutableList<Live> list;

    public AllLives(ImmutableList<Live> list) {
        this.list = Objects.requireNonNull(list);
    }

    public ImmutableList<Live> getList() {
        return list;
    }
}
