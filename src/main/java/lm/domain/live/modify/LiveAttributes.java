package lm.domain.live.modify;

import lm.domain.Id;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;
import lm.domain.master.venue.Venue;

import java.util.Objects;
import java.util.Optional;

public class LiveAttributes {
    private final LiveName name;
    private final LiveHomePageUrl homePageUrl;
    private final Id<Venue> venueId;

    public LiveAttributes(LiveName name, LiveHomePageUrl homePageUrl, Id<Venue> venueId) {
        this.name = Objects.requireNonNull(name);
        this.homePageUrl = Objects.requireNonNull(homePageUrl);
        this.venueId = venueId;
    }

    public LiveName getName() {
        return name;
    }

    public LiveHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }

    public Optional<Id<Venue>> getVenueId() {
        return Optional.ofNullable(venueId);
    }

    @Override
    public String toString() {
        return "LiveAttribute{" +
                "name=" + name +
                ", homePageUrl=" + homePageUrl +
                ", venueId=" + venueId +
                '}';
    }
}
