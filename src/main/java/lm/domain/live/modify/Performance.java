package lm.domain.live.modify;

import lm.domain.DomainMessage;
import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;

import java.io.Serializable;
import java.util.Objects;

/**
 * 新しく登録される公演.
 */
public class Performance implements Serializable {
    private final Id<Performance> id;
    private final PerformanceDate date;
    private final OpenTime openTime;
    
    private final StartTime startTime;
    
    public static Performance newPerformance(PerformanceDate date, OpenTime openTime, StartTime startTime) {
        return new Performance(date, openTime, startTime);
    }
    
    public static Performance rebuild(Id<Performance> id, PerformanceDate date, OpenTime openTime, StartTime startTime) {
        return new Performance(id, date, openTime, startTime);
    }

    private Performance(Id<Performance> id, PerformanceDate date, OpenTime openTime, StartTime startTime) {
        this.validateRelation(openTime, startTime);
        
        this.id = Objects.requireNonNull(id);
        this.date = Objects.requireNonNull(date);
        this.openTime = Objects.requireNonNull(openTime);
        this.startTime = Objects.requireNonNull(startTime);
    }

    private Performance(PerformanceDate date, OpenTime openTime, StartTime startTime) {
        this.validateRelation(openTime, startTime);
        
        this.id = null;
        this.date = Objects.requireNonNull(date);
        this.openTime = Objects.requireNonNull(openTime);
        this.startTime = Objects.requireNonNull(startTime);
    }

    private void validateRelation(OpenTime openTime, StartTime startTime) {
        if (!openTime.isBefore(startTime)) {
            throw new DomainException(DomainMessage.OPEN_DATE_TIME_MUST_BE_BEFORE_THAN_START_DATE_TIME);
        }
    }
    
    public boolean hasSameKey(Performance other) {
        return this.date.equals(other.date)
                && this.startTime.equals(other.startTime);
    }
    
    public boolean hasSameId(Id<Performance> id) {
        return this.id.equals(id);
    }

    public Id<Performance> getId() {
        return id;
    }

    public PerformanceDate getDate() {
        return date;
    }

    public OpenTime getOpenTime() {
        return openTime;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    @Override
    public String toString() {
        return "Performance{" +
                "date=" + date +
                ", openTime=" + openTime +
                ", startTime=" + startTime +
                '}';
    }
}
