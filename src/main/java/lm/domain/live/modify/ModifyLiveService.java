package lm.domain.live.modify;

import lm.domain.DomainMessage;
import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.LiveRepository;
import org.springframework.stereotype.Component;

@Component
public class ModifyLiveService {
    
    private final LiveRepository liveRepository;

    public ModifyLiveService(LiveRepository liveRepository) {
        this.liveRepository = liveRepository;
    }
    
    public void modify(Id<Live> liveId, LiveAttributes liveAttributes) {
        if (this.liveRepository.existsSameNameLive(liveAttributes.getName(), liveId)) {
            throw new DomainException(DomainMessage.LIVE_NAME_IS_ALREADY_USED);
        }
        
        this.liveRepository.modify(liveId, liveAttributes);
    }
    
    public void addPerformance(Id<Live> liveId, Performance performance) {
        Live live = this.liveRepository.searchWithLock(liveId);
        live.add(performance);
        this.liveRepository.addPerformance(liveId, performance);
    }
    
    public void removePerformance(Id<Live> liveId, Id<Performance> performanceId) {
        Live live = this.liveRepository.searchWithLock(liveId);
        if (live.remove(performanceId)) {
            this.liveRepository.removePerformance(performanceId);
        }
    }
}
