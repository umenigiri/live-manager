package lm.domain.live.modify;

import org.eclipse.collections.api.list.ImmutableList;

import java.util.Objects;

/**
 * 更新対象以外の公演一覧.
 */
public class OtherPerformances {
    private final ImmutableList<Performance> list;

    public OtherPerformances(ImmutableList<Performance> list) {
        this.list = Objects.requireNonNull(list);
    }

    public ImmutableList<Performance> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "OtherPerformances{" +
                "list=" + list +
                '}';
    }
}
