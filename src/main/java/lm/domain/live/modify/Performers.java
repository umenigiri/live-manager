package lm.domain.live.modify;

import lm.domain.master.artist.Artist;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

import java.io.Serializable;
import java.util.Objects;

/**
 * １つの公演に出演する出演者の一覧.
 */
public class Performers implements Serializable {
    private final MutableList<Artist> values = Lists.mutable.of();
    
    void add(Artist artist) {
        this.values.add(Objects.requireNonNull(artist));
    }
}
