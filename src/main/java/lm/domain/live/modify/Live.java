package lm.domain.live.modify;

import lm.domain.Id;

import java.io.Serializable;
import java.util.Objects;

/**
 * ライブ.
 */
public class Live implements Serializable {
    private final Id<Live> id;
    private final LiveAttributes attributes;
    private final Performances performances;

    public Live(Id<Live> id, LiveAttributes attributes, Performances performances) {
        this.id = Objects.requireNonNull(id);
        this.attributes = Objects.requireNonNull(attributes);
        this.performances = Objects.requireNonNull(performances);
    }

    public void add(Performance performance) {
        this.performances.add(performance);
    }

    public boolean remove(Id<Performance> performanceId) {
        return this.performances.remove(performanceId);
    }
    
    public Performance get(Id<Performance> performanceId) {
        return this.performances.get(performanceId);
    }
    
    public OtherPerformances getOtherPerformances(Id<Performance> excludePerformanceId) {
        return this.performances.getOtherPerformances(excludePerformanceId);
    }

    public Id<Live> getId() {
        return id;
    }

    public LiveAttributes getAttributes() {
        return attributes;
    }

    public Performances getPerformances() {
        return performances;
    }

    @Override
    public String toString() {
        return "Live{" +
                "id=" + id +
                ", attributes=" + attributes +
                ", performances=" + performances +
                '}';
    }

    public void modify(Performance performance) {
        this.performances.modify(performance);
    }
}
