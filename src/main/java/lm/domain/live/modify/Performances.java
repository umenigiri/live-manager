package lm.domain.live.modify;

import lm.domain.DomainMessage;
import lm.domain.Id;
import lm.domain.exception.DomainException;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * １つのライブで行われる全公演.
 */
public class Performances implements Serializable {
    private final MutableList<Performance> values;
    
    void add(Performance performance) {
        if (this.values.anySatisfy(p -> p.hasSameKey(performance))) {
            throw new DomainException(DomainMessage.PERFORMANCE_KEY_IS_DUPLICATED);
        }
        this.values.add(Objects.requireNonNull(performance));
    }

    boolean remove(Id<Performance> performanceId) {
        return this.values.removeIf(p -> p.hasSameId(performanceId));
    }

    void modify(Performance performance) {
        Performances copy = new Performances(Lists.immutable.ofAll(this.values));
        this.invokeModify(copy, performance);
        this.invokeModify(this, performance);
    }
    
    private void invokeModify(Performances performances, Performance performance) {
        if (!performances.remove(performance.getId())) {
            throw new DomainException(DomainMessage.PERFORMANCE_WAS_REMOVED);
        }
        performances.add(performance);
    }
    
    public Performances(ImmutableList<Performance> performances) {
        this.values = performances.toList();
    }

    public List<Performance> getList() {
        return this.values;
    }

    public Performance get(Id<Performance> performanceId) {
        return this.values.detectOptional(p -> p.hasSameId(performanceId))
                .orElseThrow(() -> new DomainException(DomainMessage.PERFORMANCE_WAS_REMOVED));
    }

    OtherPerformances getOtherPerformances(Id<Performance> excludePerformanceId) {
        return new OtherPerformances(this.values.reject(p -> p.hasSameId(excludePerformanceId)).toImmutable());
    }
}
