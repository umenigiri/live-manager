package lm.domain.live.modify;

import lm.domain.Id;
import lm.domain.live.LiveRepository;
import org.springframework.stereotype.Component;

@Component
public class ModifyPerformanceService {
    
    private final LiveRepository liveRepository;

    public ModifyPerformanceService(LiveRepository liveRepository) {
        this.liveRepository = liveRepository;
    }
    
    public void modify(Id<Live> liveId, Performance performance) {
        Live live = this.liveRepository.searchWithLock(liveId);
        live.modify(performance);
        
        this.liveRepository.modifyPerformance(performance);
    }
}
