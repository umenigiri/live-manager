package lm.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * エンティティのID.
 * @param <T> エンティティの型
 */
public class Id<T> implements Serializable {
    private final long value;

    public Id(long value) {
        if (value <= 0) {
            throw new IllegalArgumentException("id は 1 以上. value=" + value);
        }
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Id{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Id<?> id = (Id<?>) o;
        return value == id.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
