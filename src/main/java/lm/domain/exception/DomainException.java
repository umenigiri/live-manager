package lm.domain.exception;

import lm.domain.DomainMessage;

/**
 * ドメインルールの違反を表す例外.
 */
public class DomainException extends RuntimeException {
    
    @Deprecated
    public DomainException(String message) {
        super(message);
    }
    
    public DomainException(DomainMessage message) {
        super(message.build());
    }
}
