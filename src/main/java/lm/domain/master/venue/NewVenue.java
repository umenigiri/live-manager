package lm.domain.master.venue;

/**
 * 新規登録される会場.
 */
public class NewVenue {
    private final VenueName name;
    private final VenueAddress address;
    private final VenueHomePageUrl homePageUrl;

    public NewVenue(VenueName name, VenueAddress address, VenueHomePageUrl homePageUrl) {
        this.name = name;
        this.address = address;
        this.homePageUrl = homePageUrl;
    }

    public VenueName getName() {
        return name;
    }

    public VenueAddress getAddress() {
        return address;
    }

    public VenueHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }
    
    @Override
    public String toString() {
        return "NewVenue{" +
                "name=" + name +
                ", address=" + address +
                ", homePageUrl=" + homePageUrl +
                '}';
    }

}
