package lm.domain.master.venue;

import lm.domain.DomainMessage;
import lm.domain.exception.DomainException;
import org.springframework.stereotype.Component;

@Component
public class RegisterVenueService {
    private final VenueRepository venueRepository;

    public RegisterVenueService(VenueRepository venueRepository) {
        this.venueRepository = venueRepository;
    }
    
    public void register(NewVenue newVenue) {
        if (this.venueRepository.existsSameNameVenue(newVenue.getName())) {
            throw new DomainException(DomainMessage.VENUE_NAME_IS_ALREADY_USED);
        }
        
        this.venueRepository.register(newVenue);
    }
}
