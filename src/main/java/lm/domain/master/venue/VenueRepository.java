package lm.domain.master.venue;

import lm.domain.Id;

public interface VenueRepository {

    /**
     * 同じ名前の会場が存在しないか確認する.
     * @param venueName 検証する会場名
     * @return 同名の会場がすでに存在する場合は {@code true}
     */
    boolean existsSameNameVenue(VenueName venueName);

    /**
     * 指定した ID 以外で、同じ名前の会場が存在しないか確認する.
     * @param venueName 検証する会場名
     * @param excludeId 除外する会場の ID
     * @return 同名の会場がすでに存在する場合は {@code true}
     */
    boolean existsSameNameVenue(VenueName venueName, Id<Venue> excludeId);

    /**
     * ID で検索する.
     * @param id ID
     * @return 検索結果
     */
    Venue findById(Id<Venue> id);

    /**
     * 全ての会場を取得する.
     * @return 全会場
     */
    AllVenues findAll();

    /**
     * 新しい会場を登録する.
     * @param newVenue 新しい会場
     */
    void register(NewVenue newVenue);

    /**
     * 会場の情報を更新する.
     * @param venue 更新情報
     */
    void modify(Venue venue);
}
