package lm.domain.master.venue;

import lm.domain.DomainMessage;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * ライブ会場のホームページ URL.
 */
public class VenueHomePageUrl implements Serializable {
    private final String value;

    public VenueHomePageUrl(String value) {
        assertNotNull(value);
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueHomePageUrl that = (VenueHomePageUrl) o;
        return Objects.equals(value, that.value);
    }

    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "VenueHomePageUrl{" +
                "value='" + value + '\'' +
                '}';
    }

    @Size(max = 500, message = DomainMessage.INVALID_VENUE_HOME_PAGE_URL_LENGTH)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
