package lm.domain.master.venue;

import lm.domain.DomainMessage;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * ライブ会場の住所.
 */
public class VenueAddress implements Serializable {
    private final String value;

    public VenueAddress(String value) {
        assertNotNull(value);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VenueAddress that = (VenueAddress) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "VenueAddress{" +
                "value='" + value + '\'' +
                '}';
    }
    
    @Size(max = 500, message = DomainMessage.INVALID_VENUE_ADDRESS_LENGTH)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
