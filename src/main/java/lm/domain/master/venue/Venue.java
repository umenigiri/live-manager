package lm.domain.master.venue;

import lm.domain.Id;

import java.io.Serializable;
import java.util.Objects;

/**
 * ライブが開催される会場.
 * <p>
 * 「さいたまスーパーアリーナ」「武道館」など.
 */
public class Venue implements Serializable {
    private final Id<Venue> id;
    private final VenueName name;
    private final VenueAddress address;
    private final VenueHomePageUrl homePageUrl;

    public Venue(Id<Venue> id, VenueName name, VenueAddress address, VenueHomePageUrl homePageUrl) {
        this.id = Objects.requireNonNull(id);
        this.name = Objects.requireNonNull(name);
        this.address = Objects.requireNonNull(address);
        this.homePageUrl = Objects.requireNonNull(homePageUrl);
    }

    public Id<Venue> getId() {
        return id;
    }

    public VenueName getName() {
        return name;
    }

    public VenueAddress getAddress() {
        return address;
    }

    public VenueHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }


    @Override
    public String toString() {
        return "Venue{" +
                "id=" + id +
                ", name=" + name +
                ", address=" + address +
                ", homePageUrl=" + homePageUrl +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venue venue = (Venue) o;
        return Objects.equals(address, venue.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
