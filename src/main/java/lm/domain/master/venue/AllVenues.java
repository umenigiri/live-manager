package lm.domain.master.venue;

import org.eclipse.collections.api.list.ImmutableList;

import java.util.Objects;

/**
 * 登録済みの全会場.
 */
public class AllVenues {
    private final ImmutableList<Venue> list;

    public AllVenues(ImmutableList<Venue> list) {
        this.list = Objects.requireNonNull(list);
    }

    public ImmutableList<Venue> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "AllVenues{" +
                "list=" + list +
                '}';
    }
}
