package lm.domain.master.venue;

import lm.domain.DomainMessage;
import lm.domain.exception.DomainException;
import org.springframework.stereotype.Component;

@Component
public class ModifyVenueService {
    private final VenueRepository venueRepository;

    public ModifyVenueService(VenueRepository venueRepository) {
        this.venueRepository = venueRepository;
    }
    
    public void modify(Venue venue) {
        if (this.venueRepository.existsSameNameVenue(venue.getName(), venue.getId())) {
            throw new DomainException(DomainMessage.VENUE_NAME_IS_ALREADY_USED);
        }
        
        this.venueRepository.modify(venue);
    }
}
