package lm.domain.master.artist;

import lm.domain.DomainMessage;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Objects;

import static lm.domain.ValueObjectAssertion.*;

/**
 * アーティストの名前.
 */
public class ArtistName implements Serializable {
    private final String value;

    public ArtistName(String value) {
        assertNotBlank(value);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtistName that = (ArtistName) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "ArtistName{" +
                "value='" + value + '\'' +
                '}';
    }
    
    @Size(max = 100, message = DomainMessage.INVALID_ARTIST_NAME_LENGTH)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Constraint(validatedBy = {})
    public static @interface Validation {
        String message() default "";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}
