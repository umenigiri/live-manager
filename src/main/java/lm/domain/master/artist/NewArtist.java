package lm.domain.master.artist;

import java.util.Objects;

/**
 * 新規登録されるアーティスト.
 */
public class NewArtist {
    private final ArtistName name;
    private final ArtistHomePageUrl homePageUrl;

    public NewArtist(ArtistName name, ArtistHomePageUrl homePageUrl) {
        this.name = Objects.requireNonNull(name);
        this.homePageUrl = Objects.requireNonNull(homePageUrl);
    }

    public ArtistName getName() {
        return name;
    }

    public ArtistHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }

    @Override
    public String toString() {
        return "NewArtist{" +
                "name=" + name +
                ", homePageUrl=" + homePageUrl +
                '}';
    }
}
