package lm.domain.master.artist;

import lm.domain.DomainMessage;
import lm.domain.exception.DomainException;
import org.springframework.stereotype.Component;

@Component
public class ModifyArtistService {
    private final ArtistRepository artistRepository;

    public ModifyArtistService(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }
    
    public void modify(Artist artist) {
        if (this.artistRepository.existsSameNameArtist(artist.getId(), artist.getName())) {
            throw new DomainException(DomainMessage.ARTIST_NAME_IS_ALREADY_USED);
        }
        
        this.artistRepository.modify(artist);
    }
}
