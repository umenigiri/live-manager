package lm.domain.master.artist;

import lm.domain.DomainMessage;
import lm.domain.exception.DomainException;
import org.springframework.stereotype.Component;

@Component
public class RegisterNewArtistService {
    private final ArtistRepository artistRepository;

    public RegisterNewArtistService(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    public void register(NewArtist newArtist) {
        if (this.artistRepository.existsSameNameArtist(newArtist.getName())) {
            throw new DomainException(DomainMessage.ARTIST_NAME_IS_ALREADY_USED);
        }
        
        this.artistRepository.register(newArtist);
    }
}
