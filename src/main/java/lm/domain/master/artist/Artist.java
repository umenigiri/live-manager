package lm.domain.master.artist;

import lm.domain.Id;

import java.io.Serializable;
import java.util.Objects;

/**
 * 登録済みのアーティストの参照.
 */
public class Artist implements Serializable {
    private final Id<Artist> id;
    private final ArtistName name;
    private final ArtistHomePageUrl homePageUrl;

    public Artist(Id<Artist> id, ArtistName name, ArtistHomePageUrl homePageUrl) {
        this.id = Objects.requireNonNull(id);
        this.name = Objects.requireNonNull(name);
        this.homePageUrl = Objects.requireNonNull(homePageUrl);
    }

    public Id<Artist> getId() {
        return id;
    }

    public ArtistName getName() {
        return name;
    }

    public ArtistHomePageUrl getHomePageUrl() {
        return homePageUrl;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name=" + name +
                ", homePageUrl=" + homePageUrl +
                '}';
    }
}
