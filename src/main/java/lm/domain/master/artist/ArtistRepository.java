package lm.domain.master.artist;

import lm.domain.Id;

/**
 * アーティストのリポジトリ.
 */
public interface ArtistRepository {

    /**
     * 同じ名前のアーティストが存在するか確認し、存在する場合は {@code true} を返す.
     * @param artistName ここで指定した名前のアーティストがすでに存在しないか確認する
     * @return 同名のアーティストが存在する場合は {@code true}
     */
    boolean existsSameNameArtist(ArtistName artistName);

    /**
     * 指定した ID 以外で同じ名前のアーティストがすでに存在しないか確認する.
     * <p>
     * このメソッドはアーティストの名前を変更したときの重複チェックのために用います.<br>
     * 名前の更新の場合、更新対象自身のアーティストは重複検証の対象外に除外する必要があります.<br>
     * このため、登録時とは異なるメソッドが提供されています.
     * 
     * @param id 検証の除外対象となるアーティストのID
     * @param artistName 重複を確認するアーティストの名前
     * @return 同じ名前がすでに存在する場合は {@code true}
     */
    boolean existsSameNameArtist(Id<Artist> id, ArtistName artistName);

    /**
     * 全てのアーティストを検索する.
     * @return 全アーティスト
     */
    Artists findAll();

    /**
     * 指定した ID のアーティスト情報を検索する.
     * @param id アーティストの ID
     * @return 検索結果のアーティスト
     */
    Artist findById(Id<Artist> id);
    
    /**
     * 新しいアーティストを登録する.
     * @param newArtist 新しいアーティスト
     */
    void register(NewArtist newArtist);

    /**
     * アーティストの情報を更新する.
     * @param artist 更新内容を持つアーティスト
     */
    void modify(Artist artist);
}
