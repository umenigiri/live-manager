package lm.domain.master.artist;

import org.eclipse.collections.api.list.ImmutableList;

import java.util.Objects;

/**
 * {@link Artist} の一覧.
 */
public class Artists {
    private final ImmutableList<Artist> list;

    public Artists(ImmutableList<Artist> list) {
        this.list = Objects.requireNonNull(list);
    }

    public ImmutableList<Artist> getList() {
        return list;
    }
}
