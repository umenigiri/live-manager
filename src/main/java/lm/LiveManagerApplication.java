package lm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiveManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiveManagerApplication.class, args);
    }

}

