package lm.application.register;

import lm.domain.live.register.NewLive;
import lm.domain.live.LiveRepository;

public class RegisterNewLiveService {
    private final LiveRepository liveRepository;

    public RegisterNewLiveService(LiveRepository liveRepository) {
        this.liveRepository = liveRepository;
    }

    public void execute(NewLive newLive) {
        this.liveRepository.register(newLive);
    }
}
