package lm.presentation.web.page.live.modify;

import lm.domain.live.LiveRepository;
import lm.domain.live.modify.AllLives;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/live")
public class LiveListController {
    private static final String TEMPLATE_PATH = "live/live-list";
    
    private final LiveRepository liveRepository;

    public LiveListController(LiveRepository liveRepository) {
        this.liveRepository = liveRepository;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public AllLives allLives() {
        return this.liveRepository.getAllLives();
    }
}
