package lm.presentation.web.page.live.register;

import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;
import lm.domain.live.modify.Live;
import lm.domain.live.register.NewLive;
import lm.domain.live.register.RegisterLiveService;
import lm.domain.master.venue.AllVenues;
import lm.domain.master.venue.Venue;
import lm.domain.master.venue.VenueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("/live/register")
public class RegisterLiveController {
    private static final Logger logger = LoggerFactory.getLogger(RegisterLiveController.class);
    private static final String TEMPLATE_PATH = "live/register-live";
    
    private final VenueRepository venueRepository;
    private final RegisterLiveService registerLiveService;

    public RegisterLiveController(VenueRepository venueRepository, RegisterLiveService registerLiveService) {
        this.venueRepository = venueRepository;
        this.registerLiveService = registerLiveService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public RegisterLiveForm registerLiveForm() {
        return new RegisterLiveForm();
    }
    
    @ModelAttribute
    public AllVenues allVenues() {
        return this.venueRepository.findAll();
    }
    
    @PostMapping
    public String register(@Valid RegisterLiveForm form, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("register form={}", form);
        
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }
        
        LiveName liveName = new LiveName(form.name);
        Id<Venue> venueId = form.getVenueId().map(Id<Venue>::new).orElse(null);
        LiveHomePageUrl liveHomePageUrl = new LiveHomePageUrl(form.homePageUrl);
        NewLive newLive = new NewLive(liveName, venueId, liveHomePageUrl);

        try {
            Id<Live> liveId = this.registerLiveService.register(newLive);
            
            redirectAttributes.addFlashAttribute("message", "登録しました。");

            return "redirect:/live/" + liveId.getValue();
        } catch (DomainException e) {
            logger.warn(e.getMessage());
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
