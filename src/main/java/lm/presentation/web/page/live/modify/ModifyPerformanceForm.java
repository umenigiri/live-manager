package lm.presentation.web.page.live.modify;

import lm.domain.DomainMessage;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;

import javax.validation.constraints.NotBlank;

public class ModifyPerformanceForm {
    @NotBlank(message = DomainMessage.PERFORMANCE_DATE_IS_REQUIRED)
    @PerformanceDate.Validation
    public String date;

    @OpenTime.HourValidation
    public int openHour;
    @OpenTime.MinuteValidation
    public int openMinute;

    @StartTime.HourValidation
    public int startHour;
    @StartTime.MinuteValidation
    public int startMinute;

    @Override
    public String toString() {
        return "ModifyPerformanceForm{" +
                "date='" + date + '\'' +
                ", openHour=" + openHour +
                ", openMinute=" + openMinute +
                ", startHour=" + startHour +
                ", startMinute=" + startMinute +
                '}';
    }
}
