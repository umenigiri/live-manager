package lm.presentation.web.page.live.modify;

import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.LiveRepository;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;
import lm.domain.live.modify.Live;
import lm.domain.live.modify.ModifyPerformanceService;
import lm.domain.live.modify.Performance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
@RequestMapping("/live/{liveId}/performance/{performanceId}")
public class ModifyPerformanceController {
    private static final Logger logger = LoggerFactory.getLogger(ModifyPerformanceController.class);
    private static final String TEMPLATE_PATH = "live/modify-performance";
    
    private final LiveRepository liveRepository;
    private final ModifyPerformanceService modifyPerformanceService;

    public ModifyPerformanceController(LiveRepository liveRepository, ModifyPerformanceService modifyPerformanceService) {
        this.liveRepository = liveRepository;
        this.modifyPerformanceService = modifyPerformanceService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }

    @ModelAttribute
    public void page(Model model, @PathVariable long liveId, @PathVariable long performanceId) {
        Live live = this.liveRepository.search(new Id<>(liveId));
        model.addAttribute("liveName", live.getAttributes().getName().getValue());

        try {
            ModifyPerformanceForm form = new ModifyPerformanceForm();
            Performance performance = live.get(new Id<>(performanceId));
            form.date = performance.getDate().format();
            form.openHour = performance.getOpenTime().getHour();
            form.openMinute = performance.getOpenTime().getMinute();
            form.startHour = performance.getStartTime().getHour();
            form.startMinute = performance.getStartTime().getMinute();
            model.addAttribute(form);
            
            model.addAttribute(live.getOtherPerformances(performance.getId()));
        } catch (DomainException e) {
            model.addAttribute(e);
        }
    }
    
    @PostMapping(params = "modify")
    public String modify(
        ModifyPerformanceForm form,
        BindingResult bindingResult,
        @PathVariable long liveId,
        @PathVariable long performanceId,
        Model model,
        RedirectAttributes redirectAttributes
    ) {
        logger.debug("[Modify-Performance] liveId={}, performanceId={}, form={}", liveId, performanceId, form);
        
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        try {
            PerformanceDate performanceDate = PerformanceDate.parse(form.date);
            OpenTime openTime = new OpenTime(form.openHour, form.openMinute);
            StartTime startTime = new StartTime(form.startHour, form.startMinute);
            Performance performance = Performance.rebuild(new Id<>(performanceId), performanceDate, openTime, startTime);
            this.modifyPerformanceService.modify(new Id<>(liveId), performance);
            
            redirectAttributes.addFlashAttribute("message", "公演を更新しました。");
            return "redirect:/live/{liveId}/performance/{performanceId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
