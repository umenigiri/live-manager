package lm.presentation.web.page.live.register;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

/**
 * ライブ登録画面の各公演の情報を保持した Form.
 */
public class RegisterLivePerformanceForm {
    public String date;
    public String openTime;
    public String startTime;
    
    public Optional<String> getDate() {
        if (StringUtils.isBlank(this.date)) {
            return Optional.empty();
        } else {
            return Optional.of(this.date);
        }
    }

    public Optional<String> getOpenTime() {
        if (StringUtils.isBlank(this.openTime)) {
            return Optional.empty();
        } else {
            return Optional.of(this.openTime);
        }
    }

    public Optional<String> getStartTime() {
        if (StringUtils.isBlank(this.startTime)) {
            return Optional.empty();
        } else {
            return Optional.of(this.startTime);
        }
    }

    @Override
    public String toString() {
        return "RegisterLivePerformanceForm{" +
                "date='" + date + '\'' +
                ", openTime='" + openTime + '\'' +
                ", startTime='" + startTime + '\'' +
                '}';
    }
}
