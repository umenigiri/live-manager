package lm.presentation.web.page.live.modify;

import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;
import lm.domain.live.LiveRepository;
import lm.domain.live.OpenTime;
import lm.domain.live.PerformanceDate;
import lm.domain.live.StartTime;
import lm.domain.live.modify.Live;
import lm.domain.live.modify.LiveAttributes;
import lm.domain.live.modify.ModifyLiveService;
import lm.domain.live.modify.Performance;
import lm.domain.live.modify.Performances;
import lm.domain.master.venue.AllVenues;
import lm.domain.master.venue.Venue;
import lm.domain.master.venue.VenueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("/live/{liveId}")
public class ModifyLiveController {
    private static final Logger logger = LoggerFactory.getLogger(ModifyLiveController.class);
    private static final String TEMPLATE_PATH = "live/modify-live";
    
    private final VenueRepository venueRepository;
    private final LiveRepository liveRepository;
    private final ModifyLiveService modifyLiveService;

    public ModifyLiveController(VenueRepository venueRepository, LiveRepository liveRepository, ModifyLiveService modifyLiveService) {
        this.venueRepository = venueRepository;
        this.liveRepository = liveRepository;
        this.modifyLiveService = modifyLiveService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public ModifyLiveForm modifyLiveForm(@PathVariable long liveId) {
        ModifyLiveForm form = new ModifyLiveForm();

        Live live = this.liveRepository.search(new Id<>(liveId));
        form.id = liveId;
        form.name = live.getAttributes().getName().getValue();
        form.homePageUrl = live.getAttributes().getHomePageUrl().getValue();
        form.venueId = live.getAttributes().getVenueId().map(Id::getValue).orElse(0L);

        return form;
    }
    
    @ModelAttribute
    public AddPerformanceForm addPerformanceForm() {
        return new AddPerformanceForm();
    }
    
    @ModelAttribute
    public AllVenues allVenues() {
        return this.venueRepository.findAll();
    }
    
    @ModelAttribute
    public Performances performances(@PathVariable long liveId) {
        return this.liveRepository.search(new Id<>(liveId)).getPerformances();
    }
    
    @PostMapping(params = "modify")
    public String modify(
        @Valid ModifyLiveForm form,
        BindingResult bindingResult,
        Model model,
        @PathVariable long liveId,
        RedirectAttributes redirectAttributes
    ) {
        logger.debug("[Modify] liveId = {}, form = {}", liveId, form);
        
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        Id<Live> id = new Id<>(liveId);
        LiveName liveName = new LiveName(form.name);
        LiveHomePageUrl liveHomePageUrl = new LiveHomePageUrl(form.homePageUrl);
        Id<Venue> venueId = form.getVenueId().map(Id<Venue>::new).orElse(null);
        LiveAttributes liveAttributes = new LiveAttributes(liveName, liveHomePageUrl, venueId);
        
        try {
            this.modifyLiveService.modify(id, liveAttributes);
            redirectAttributes.addFlashAttribute("message", "更新しました");
            
            return "redirect:/live/{liveId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
    
    @PostMapping(params = "add-performance")
    public String addPerformance(
        @Valid AddPerformanceForm form,
        BindingResult bindingResult,
        Model model,
        @PathVariable long liveId,
        RedirectAttributes redirectAttributes
    ) {
        logger.debug("[Add-Performance] liveId = {}, form = {}", liveId, form);
        
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        Id<Live> id = new Id<>(liveId);
        PerformanceDate performanceDate = PerformanceDate.parse(form.date);
        OpenTime openTime = new OpenTime(form.openHour, form.openMinute);
        StartTime startTime = new StartTime(form.startHour, form.startMinute);
        
        try {
            Performance performance = Performance.newPerformance(performanceDate, openTime, startTime);
            this.modifyLiveService.addPerformance(id, performance);

            redirectAttributes.addFlashAttribute("message", "公演を追加しました");
            
            return "redirect:/live/{liveId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
    
    @PostMapping(params = "remove-performance")
    public String removePerformance(
        Model model,
        @PathVariable long liveId,
        @RequestParam("remove-performance") long performanceId,
        RedirectAttributes redirectAttributes
    ) {
        logger.debug("[Remove-Performance] liveId = {}, performanceId = {}", liveId, performanceId);

        try {
            this.modifyLiveService.removePerformance(new Id<>(liveId), new Id<>(performanceId));

            redirectAttributes.addFlashAttribute("message", "公演を削除しました");

            return "redirect:/live/{liveId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
