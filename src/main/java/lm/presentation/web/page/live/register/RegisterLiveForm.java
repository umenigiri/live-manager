package lm.presentation.web.page.live.register;

import lm.domain.DomainMessage;
import lm.domain.live.LiveHomePageUrl;
import lm.domain.live.LiveName;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

public class RegisterLiveForm {
    @NotBlank(message = DomainMessage.LIVE_NAME_IS_REQUIRED)
    @LiveName.Validation
    public String name;
    @LiveHomePageUrl.Validation
    public String homePageUrl;
    public long venueId;
    
    Optional<Long> getVenueId() {
        if (this.venueId <= 0L) {
            return Optional.empty();
        } else {
            return Optional.of(this.venueId);
        }
    }

    @Override
    public String toString() {
        return "RegisterLiveForm{" +
                "name='" + name + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                ", venueId=" + venueId +
                '}';
    }
}
