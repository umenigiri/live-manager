package lm.presentation.web.page.master.venue;

import lm.domain.DomainMessage;
import lm.domain.master.venue.VenueAddress;
import lm.domain.master.venue.VenueHomePageUrl;
import lm.domain.master.venue.VenueName;

import javax.validation.constraints.NotBlank;

public class RegisterVenueForm {
    @NotBlank(message = DomainMessage.VENUE_NAME_IS_REQUIRED)
    @VenueName.Validation
    public String name;
    @VenueAddress.Validation
    public String address;
    @VenueHomePageUrl.Validation
    public String homePageUrl;

    @Override
    public String toString() {
        return "RegisterVenueForm{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }
}
