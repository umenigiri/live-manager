package lm.presentation.web.page.master.venue;

import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.master.venue.ModifyVenueService;
import lm.domain.master.venue.Venue;
import lm.domain.master.venue.VenueAddress;
import lm.domain.master.venue.VenueHomePageUrl;
import lm.domain.master.venue.VenueName;
import lm.domain.master.venue.VenueRepository;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("/master/venue/{venueId}")
public class ModifyVenueController {
    private static final String TEMPLATE_PATH = "master/venue/modify-venue";
    
    private final VenueRepository venueRepository;
    private final ModifyVenueService modifyVenueService;

    public ModifyVenueController(VenueRepository venueRepository, ModifyVenueService modifyVenueService) {
        this.venueRepository = venueRepository;
        this.modifyVenueService = modifyVenueService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public ModifyVenueForm modifyVenueForm(@PathVariable long venueId) {
        Venue venue = this.venueRepository.findById(new Id<>(venueId));

        ModifyVenueForm form = new ModifyVenueForm();
        form.name = venue.getName().getValue();
        form.address = venue.getAddress().getValue();
        form.homePageUrl = venue.getHomePageUrl().getValue();
        
        return form;
    }
    
    @PostMapping
    public String modify(
        @Valid ModifyVenueForm form,
        @PathVariable long venueId,
        BindingResult bindingResult,
        Model model,
        RedirectAttributes redirectAttributes
    ) {
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        Id<Venue> id = new Id<>(venueId);
        VenueName venueName = new VenueName(form.name);
        VenueAddress venueAddress = new VenueAddress(form.address);
        VenueHomePageUrl venueHomePageUrl = new VenueHomePageUrl(form.homePageUrl);
        Venue venue = new Venue(id, venueName, venueAddress, venueHomePageUrl);

        try {
            this.modifyVenueService.modify(venue);
            redirectAttributes.addFlashAttribute("message", "更新が完了しました");
            return "redirect:/master/venue/{venueId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
