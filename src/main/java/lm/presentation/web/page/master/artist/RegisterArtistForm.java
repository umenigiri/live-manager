package lm.presentation.web.page.master.artist;

import lm.domain.DomainMessage;
import lm.domain.master.artist.ArtistHomePageUrl;
import lm.domain.master.artist.ArtistName;

import javax.validation.constraints.NotBlank;

public class RegisterArtistForm {
    @NotBlank(message = DomainMessage.ARTIST_NAME_IS_REQUIRED)
    @ArtistName.Validation
    public String name;
    
    @ArtistHomePageUrl.Validation
    public String homePageUrl;

    @Override
    public String toString() {
        return "RegisterArtistForm{" +
                "name='" + name + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }
}
