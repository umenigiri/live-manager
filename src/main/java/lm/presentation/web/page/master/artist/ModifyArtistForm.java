package lm.presentation.web.page.master.artist;

import lm.domain.DomainMessage;
import lm.domain.master.artist.ArtistHomePageUrl;
import lm.domain.master.artist.ArtistName;

import javax.validation.constraints.NotBlank;

public class ModifyArtistForm {
    @NotBlank(message = DomainMessage.ARTIST_NAME_IS_REQUIRED)
    @ArtistName.Validation
    public String name;
    @ArtistHomePageUrl.Validation
    public String homePageUrl;
    
    @Override
    public String toString() {
        return "ModifyArtistForm{" +
                "name='" + name + '\'' +
                ", homePageUrl='" + homePageUrl + '\'' +
                '}';
    }

}
