package lm.presentation.web.page.master.artist;

import lm.domain.exception.DomainException;
import lm.domain.master.artist.ArtistHomePageUrl;
import lm.domain.master.artist.ArtistName;
import lm.domain.master.artist.ArtistRepository;
import lm.domain.master.artist.Artists;
import lm.domain.master.artist.NewArtist;
import lm.domain.master.artist.RegisterNewArtistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Transactional
@Controller
@RequestMapping("/master/artist")
public class RegisterArtistController {
    private static final String TEMPLATE_PATH = "master/artist/register-artist";
    private static final Logger logger = LoggerFactory.getLogger(RegisterArtistController.class);
    
    private final RegisterNewArtistService registerNewArtistService;
    private final ArtistRepository artistRepository;

    public RegisterArtistController(RegisterNewArtistService registerNewArtistService, ArtistRepository artistRepository) {
        this.registerNewArtistService = registerNewArtistService;
        this.artistRepository = artistRepository;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public Artists artists() {
        return this.artistRepository.findAll();
    }
    
    @ModelAttribute
    public RegisterArtistForm registerArtistForm() {
        return new RegisterArtistForm();
    }
        
    @PostMapping
    public String register(@Valid RegisterArtistForm form, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("form = {}", form);
        
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        ArtistName artistName = new ArtistName(form.name);
        ArtistHomePageUrl artistHomePageUrl = new ArtistHomePageUrl(form.homePageUrl);
        NewArtist newArtist = new NewArtist(artistName, artistHomePageUrl);
        try {
            this.registerNewArtistService.register(newArtist);
            
            redirectAttributes.addFlashAttribute("message", "登録が完了しました");

            return "redirect:/master/artist";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
