package lm.presentation.web.page.master.venue;

import lm.domain.exception.DomainException;
import lm.domain.master.venue.AllVenues;
import lm.domain.master.venue.NewVenue;
import lm.domain.master.venue.RegisterVenueService;
import lm.domain.master.venue.VenueAddress;
import lm.domain.master.venue.VenueHomePageUrl;
import lm.domain.master.venue.VenueName;
import lm.domain.master.venue.VenueRepository;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("/master/venue")
public class RegisterVenueController {
    private static final String TEMPLATE_PATH = "master/venue/register-venue";
    
    private final VenueRepository venueRepository;
    private final RegisterVenueService registerVenueService;

    public RegisterVenueController(VenueRepository venueRepository, RegisterVenueService registerVenueService) {
        this.venueRepository = venueRepository;
        this.registerVenueService = registerVenueService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public RegisterVenueForm registerVenueForm() {
        return new RegisterVenueForm();
    }
    
    @ModelAttribute
    public AllVenues allVenues() {
        return this.venueRepository.findAll();
    }
    
    @PostMapping
    public String register(
        @Valid RegisterVenueForm form,
        BindingResult bindingResult,
        Model model,
        RedirectAttributes redirectAttributes
    ) {
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        VenueName venueName = new VenueName(form.name);
        VenueAddress venueAddress = new VenueAddress(form.address);
        VenueHomePageUrl venueHomePageUrl = new VenueHomePageUrl(form.homePageUrl);
        NewVenue newVenue = new NewVenue(venueName, venueAddress, venueHomePageUrl);

        try {
            this.registerVenueService.register(newVenue);
            redirectAttributes.addFlashAttribute("message", "登録が完了しました");
            return "redirect:/master/venue";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
