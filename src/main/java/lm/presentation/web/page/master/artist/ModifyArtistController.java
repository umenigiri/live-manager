package lm.presentation.web.page.master.artist;

import lm.domain.Id;
import lm.domain.exception.DomainException;
import lm.domain.master.artist.Artist;
import lm.domain.master.artist.ArtistHomePageUrl;
import lm.domain.master.artist.ArtistName;
import lm.domain.master.artist.ArtistRepository;
import lm.domain.master.artist.ModifyArtistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Transactional
@Controller
@RequestMapping("/master/artist/{artistId}")
public class ModifyArtistController {
    private static final String TEMPLATE_PATH = "master/artist/modify-artist";
    private static final Logger logger = LoggerFactory.getLogger(ModifyArtistController.class);
    
    private final ArtistRepository artistRepository;
    private final ModifyArtistService modifyArtistService;

    public ModifyArtistController(ArtistRepository artistRepository, ModifyArtistService modifyArtistService) {
        this.artistRepository = artistRepository;
        this.modifyArtistService = modifyArtistService;
    }

    @GetMapping
    public String page() {
        return TEMPLATE_PATH;
    }
    
    @ModelAttribute
    public ModifyArtistForm modifyArtistForm(@PathVariable long artistId) {
        Artist artist = this.artistRepository.findById(new Id<>(artistId));
        
        ModifyArtistForm form = new ModifyArtistForm();
        form.name = artist.getName().getValue();
        form.homePageUrl = artist.getHomePageUrl().getValue();
                
        return form;
    }
    
    @PostMapping
    public String modify(
            @Valid ModifyArtistForm form,
            BindingResult bindingResult,
            Model model,
            @PathVariable long artistId,
            RedirectAttributes redirectAttributes) {
        logger.debug("modify form={}", form);
        if (bindingResult.hasErrors()) {
            return TEMPLATE_PATH;
        }

        Id<Artist> id = new Id<>(artistId);
        ArtistName artistName = new ArtistName(form.name);
        ArtistHomePageUrl artistHomePageUrl = new ArtistHomePageUrl(form.homePageUrl);
        Artist artist = new Artist(id, artistName, artistHomePageUrl);
        try {
            this.modifyArtistService.modify(artist);
            
            redirectAttributes.addFlashAttribute("message", "更新が完了しました");
            
            return "redirect:/master/artist/{artistId}";
        } catch (DomainException e) {
            model.addAttribute(e);
            return TEMPLATE_PATH;
        }
    }
}
