package lm.presentation.web.page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {
    private static final Logger logger = LoggerFactory.getLogger(CustomErrorController.class);

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object errorStatus = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        logger.debug("handleError. errorStatus={}", errorStatus);
        if (errorStatus == null) {
            return "error/unknown";
        }
        
        int statusCode = Integer.parseInt(errorStatus.toString());
        logger.debug("handleError. statusCode={}", statusCode);
        if (statusCode == 404) {
            return "error/not-found";
        } else {
            return "error/unknown";
        }
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
