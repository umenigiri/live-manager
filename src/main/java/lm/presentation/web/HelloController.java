package lm.presentation.web;

import lm.infrastructure.database.TestTable;
import lm.infrastructure.database.TestTableDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/hello")
public class HelloController {
    private final TestTableDao dao;

    public HelloController(TestTableDao dao) {
        this.dao = dao;
    }
    
    @GetMapping
    public String init(Model model) {
        model.addAttribute(new HelloForm());
        this.loadAllRecords(model);
        return "hello";
    }
    
    @PostMapping
    public String add(HelloForm form, Model model) {
        TestTable testTable = new TestTable();
        testTable.code = form.getCode();
        testTable.name = form.getName();
        this.dao.insert(testTable);
        this.loadAllRecords(model);
        return "hello";
    }
    
    private void loadAllRecords(Model model) {
        List<TestTable> records = this.dao.findAll();
        model.addAttribute("records", records);
    }
}
