package lm.presentation.web.security;

import lm.infrastructure.database.Users;
import lm.infrastructure.database.UsersDao;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    
    private final UsersDao usersDao;

    public UserDetailsServiceImpl(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = this.usersDao.findByLoginId(username).orElseThrow(() -> new UsernameNotFoundException(username + " is not found."));
        return new User(username, users.password, Set.of());
    }
}
